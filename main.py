from loreleai.reasoning.lp.prolog import SWIProlog
from loreleai.language.commons import c_pred, Predicate, Body, Atom, Clause, Procedure, Recursion
from loreleai.language.interpreters import PythonImports, PythonImport, PrettyPythonPrinter
from loreleai.learning import FillerPredicate, plain_extension, has_singleton_vars, has_duplicated_literal
from loreleai.learning.task import Task, Knowledge
from pipeline_hypothesis_space import TopDownHypothesisSpace, add_classifier, add_transformer
from pipeline_learner import SimpleBreadthFirstLearner

import sklearn
import pipeline_benchmark as benchmark


def load_estimators(name: str, print_documentation: bool = False):
    # load the requested sklearn methods from scikit-learn
    sklearn_estimators = sklearn.utils.all_estimators(type_filter=name)
    sklearn_methods = []
    for estimator in sklearn_estimators:
        length = len(str(estimator[1]).split()[1])
        module_import = str(estimator[1]).split()[1][1: length - 2]
        sklearn_methods.append((estimator[0], str(estimator[0]).lower(), module_import))
    # documentation about the supported methods
    if print_documentation:
        print("there are " + str(len(sklearn_methods)) + " " + name + " methods:")
        for classifier in sklearn_methods:
            print("- " + str(classifier[0]))
        print("")
    return sklearn_methods


def pipeline_example():
    # If print_doc is true an extensive documentation is printed out along each step.
    print_documentation = False

    # create Prolog instance
    prolog = SWIProlog()

    # read in the dataset
    if print_documentation:
        benchmark.print_dataset_information("classification", 4)
        print("")
    x, y, categorical_indicator, attribute_names = benchmark.get_dataset("classification", 4)

    # load the sklearn methods from scikit-learn
    sklearn_transformers = load_estimators("transformer", print_documentation)
    sklearn_clusters = load_estimators("cluster", print_documentation)
    sklearn_regressors = load_estimators("regressor", print_documentation)
    sklearn_classifiers = load_estimators("classifier", print_documentation)

    # define the predicates
    pipeline = c_pred("pipeline", 0)
    transformer_pred = c_pred("transformer", 1)
    cluster_pred = c_pred("cluster", 1)
    regressor_pred = c_pred("regressor", 1)
    classifier_pred = c_pred("classifier", 1)

    # specify the background knowledge
    background = Knowledge()

    # create the transformer predicates and add them to the background
    transformer_predicates = []
    for transformer in sklearn_transformers:
        predicate = transformer_pred(transformer[1])
        background.add(predicate)
        transformer_predicates.append(predicate)

    # create the cluster predicates and add them to the background
    cluster_predicates: [Predicate] = []
    for cluster in sklearn_clusters:
        predicate = cluster_pred(cluster[1])
        background.add(predicate)
        cluster_predicates.append(predicate)

    # create the classifier predicates and add them to the background
    classifier_predicates: [Predicate] = []
    for classifier in sklearn_classifiers:
        predicate = classifier_pred(classifier[1])
        background.add(predicate)
        classifier_predicates.append(predicate)

    # create the regressor predicates and add them to the background
    regressors_predicates: [Predicate] = []
    for regressor in sklearn_regressors:
        predicate = regressor_pred(regressor[1])
        background.add(predicate)
        regressors_predicates.append(predicate)

    if print_documentation:
        print("The provided background information is:")
        for transformer in sklearn_transformers:
            print(f"- {transformer_pred(transformer[1])}")
        for cluster in sklearn_clusters:
            print(f"- {cluster_pred(cluster[1])}")
        for classifier in sklearn_classifiers:
            print(f"- {classifier_pred(classifier[1])}")
        for regressors in sklearn_regressors:
            print(f"- {regressor_pred(regressors[1])}")
        print()

    # Create the constraints for the primitives
    transformer_clause_constraints = [lambda body_x: (len(body_x.get_literals()) == 0)]
    transformer_constraints = None
    classifier_clause_constraints = [lambda clause_x: (len(clause_x.get_literals()) > 0)]
    classifier_constraints = None

    # Create the primitives
    primitives = [lambda predicate_x: add_transformer(predicate_x,
                                                      transformer_predicates,
                                                      transformer_clause_constraints,
                                                      transformer_constraints),
                  lambda predicate_x: add_classifier(predicate_x,
                                                     classifier_predicates,
                                                     classifier_clause_constraints,
                                                     classifier_constraints)
                  ]

    # create the hypothesis space
    hs = TopDownHypothesisSpace(primitives=primitives,
                                head_constructor=pipeline,
                                print_documentation=print_documentation)

    learner = SimpleBreadthFirstLearner(prolog, max_body_literals=4)
    program = learner.learn(hs, 4)

    if print_documentation:
        print("\nThe program consists of the following pipelines:")
        for pipeline in program:
            print(f"- {pipeline}")

    print("\nThe program consists of the following pipelines:")
    for pipeline in program:
        print(f"- {pipeline}")


def pipeline_constructor():
    # If print_doc is true an extensive documentation is printed out along each step.
    print_documentation = False

    """
    Prolog-instance
    """
    prolog = SWIProlog()

    """
    Data-Set
    """

    """
    Foreign Functions
    """

    """
    Primitives
    """

    """
    Hypothesis-space
    """

    """
    Weights
    """

    """
    Learner
    """

    """
    Pipeline-construction
    """


if __name__ == '__main__':
    pipeline_example()
