import openml as oml
from numpy.core.multiarray import ndarray
from pandas import DataFrame, Series
from typing import Tuple, Union, List
from scipy.sparse import csr_matrix

# This benchmark contains dataset from OpenML. The data sets that are supported are:
# 44    SPAM or HAM
# 50    Tic-Tac-Toe
# 52    trains
# 56    vote
# 61    iris data set
# 455   cars
# 554   MNIST
# 1116  Musk
# 1464  blood-transfusion-service-center
# 1486  Nomao
# 1479  hill-valley
# 1489  phoneme
# 1591  Connect-4
# 40496 LED-display-domain-7digit
# 40996 Fashion-MNIST
# 41521 Weather

# The list of all dataset from OpenML that are supported:
ds_all_tasks = [44, 50, 52, 56, 61, 455, 554, 1116, 1486, 1479, 1489, 1591, 40496, 40996, 41521]
# The dataset are separated in three different categories:
ds_classification = [44, 50, 52, 56, 61, 455, 554, 1116, 1486, 1479, 1489, 1591, 40496, 40996, 41521]
ds_clustering = []
ds_regression = []


# Download the data set with the given number ds-number from OpenML and return them into dataframes
# input: ds_number: the number of the openML dataset
# output: x
# output: y
# output: categorical_indicator
# output: attribute_names
def get_dataset_from_openML(ds_number: int) \
        -> Tuple[Union[ndarray, DataFrame, csr_matrix], Union[ndarray, DataFrame, None], List[bool], List[str]]:
    ds = oml.datasets.get_dataset(ds_number)
    x, y, categorical_indicator, attribute_names = ds.get_data(
        dataset_format="dataframe", target=ds.default_target_attribute
    )
    return x, y, categorical_indicator, attribute_names


# Download the data set with the given benchmark number for the given task,
# download it OpenML and return it into a dataframe.
# input: task: a string that indicates the type of task for the dataset.
#              Currently supported: 'any' and 'classification'.
# input: ds_number: the number of the dataset for the given task.
# output: x
# output: y
# output: categorical_indicator
# output: attribute_names
def get_dataset(task: str, number: int) \
        -> Tuple[Union[ndarray, DataFrame, csr_matrix], Union[ndarray, DataFrame, None], List[bool], List[str]]:
    if task == "any":
        return get_dataset_from_openML(ds_all_tasks[number])
    elif task == "classification":
        return get_dataset_from_openML(ds_classification[number])
    # elif task == 'clustering':
    #     return get_dataset_from_openML(ds_classification[number])
    # elif task == 'regression':
    #     return get_dataset_from_openML(ds_classification[number])
    else:
        raise ValueError("Task is not supported")


# print the information of the data set with the given number from OpenML.
# input: ds_number: the number of the openML dataset
def print_dataset_from_openML_information(ds_number: int) -> None:
    # Dataset
    ds = oml.datasets.get_dataset(ds_number)

    # Information
    print(f"This is dataset '{ds.name}', the target feature is '{ds.default_target_attribute}'\n")
    print(f"URL: {ds.url}\n")
    print(f"Description: \n {ds.description}")


# print the information of the data set with the given number from the benchmark.
# input: task: a string that indicates the type of task for the dataset.
#              Currently supported: 'any' and 'classification'.
# input: number: the number of the openML dataset
def print_dataset_information(task: str, number: int) -> None:
    if task == 'any':
        print_dataset_from_openML_information(ds_all_tasks[number])
    elif task == 'classification':
        print_dataset_from_openML_information(ds_classification[number])
    # elif task == 'clustering':
    #     return print_dataset_from_openML_information(ds_classification[number])
    # elif task == 'regression':
    #     return print_dataset_from_openML_information(ds_classification[number])
    else:
        raise ValueError("Task is not supported")
